
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import React, { useEffect } from 'react';
import Footer from './components/footer/component';
import Header from './components/header/component';
import Home from './pages/Home/component';
import About from './pages/About/component';
import Courses from './pages/Courses/component';
import Events from './pages/Events/component';
import Trainers from './pages/Trainers/component';
import Contact from './pages/Contact/component';

import Python from './pages/Python/component';
import Php from './pages/Php/component';
import Angular from './pages/Angular/component';
import Dotnet from './pages/Dotnet/component';
import Android from './pages/Android/component';
import Seo from './pages/Seo/component';
import Terms from './pages/Terms/component';
import Placements from './pages/Placements/component';
import Privacy from './pages/Privacy/component';
import Node from './pages/Node/component';
import Reactnative from './pages/Reactnative/component';
import Testing from './pages/Testing/component';
import Java from './pages/Java/component';
import Datascience from './pages/Datascience/component';
import Flutter from './pages/Flutter/component';
import Machine from './pages/Machine/component';
import Testimonial from './pages/Testimonial/component';

import AOS from "aos";
import 'aos/dist/aos.css';
import Corporate from './pages/Corporate/component';
import Franchise from './pages/Franchise/component';

function App() {
  useEffect(() => {
    AOS.init({
      duration: 1000
    })
  });

  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/home" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/courses" exact component={Courses} />
          <Route path="/events" exact component={Events} />
          <Route path="/trainers" exact component={Trainers} />
          <Route path="/contact" exact component={Contact} />

          <Route path="/python" exact component={Python} />
          <Route path="/php" exact component={Php} />
          <Route path="/dotnet" exact component={Dotnet} />
          <Route path="/angular" exact component={Angular} />
          <Route path="/android" exact component={Android} />
          <Route path="/seo" exact component={Seo} />
          <Route path="/terms" exact component={Terms} />
          <Route path="/placements" exact component={Placements} />
          <Route path="/privacy" exact component={Privacy} />
          <Route path="/node" exact component={Node} />
          <Route path="/reactnative" exact component={Reactnative} />
          <Route path="/testing" exact component={Testing} />
          <Route path="/Java" exact component={Java} />
          <Route path="/datascience" exact component={Datascience} />
          <Route path="/flutter" exact component={Flutter} />
          <Route path="/machine" exact component={Machine} />
          <Route path="/testimonial" exact component={Testimonial} />
          <Route path="/franchise" exact component={Franchise} />
          <Route path="/corporate" exact component={Corporate} />
        </Switch>

        <Footer />
      </Router>
    </div>
  );
}
export default App;
