import React from 'react';
import { NavLink } from 'react-router-dom'
import './styles.scss';
import logo from "./../../assets/img/logo_new.png";


function Header(props) {
    return (
        <div>
         

            <header id="header">
    <div className="container-fluid d-flex align-items-center">

      <h1 className="logo me-auto">
        <NavLink to="/home"> 
            <img src={logo} id="logo-img" className="img-center" alt="logo"/>
        </NavLink>
      </h1>

      <nav id="navbar" className="navbar order-last order-lg-0">
        <ul>
          <li><NavLink className="active" to="/home">Home</NavLink></li>
          <li><NavLink to="/about">About</NavLink></li>
          <li><NavLink to="/courses">Courses</NavLink></li>
          <li><NavLink to="/placements">Placements</NavLink></li>
          <li><NavLink to="/testimonial">Testimonials</NavLink></li>
          <li><NavLink to="/trainers">Our Team</NavLink></li>
          <li><NavLink to="/events">Gallery</NavLink></li>
       
          <li><NavLink to="/contact">Contact</NavLink></li>
          <li><a href="https://blogs.aspireitacademy.in">Blogs</a></li>
          <li><NavLink to="/franchise">Franchising</NavLink></li>
        </ul>
        <i className="bi bi-list mobile-nav-toggle"></i>
      </nav>

      <NavLink to="/corporate" className="get-started-btn">Book Corporate Training</NavLink>

    </div>
  </header>
        </div>
    );
}

export default Header;