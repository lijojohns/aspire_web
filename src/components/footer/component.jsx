import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom'
import './styles.scss';

function Footer(props) {
    return (
        <div>
            <footer id="footer">

<div className="footer-top">
  <div className="container">
    <div className="row">

      <div className="col-lg-4 col-md-6 footer-contact">
        <h3>Aspire IT Academy</h3>
        <p>
        Innerspace, Kaloor <br/>
            Ernakulam<br/><br/>
          <strong>Phone:</strong> +91-9947244333<br/>
          <strong>Email:</strong> hr@aspireitacademy.in<br/>
        </p>
      </div>

      <div className="col-lg-4 col-md-6 footer-links">
        <h4>Useful Links</h4>
        <ul>
          <li><i className="bx bx-chevron-right"></i> <NavLink className="active" to="/home">Home</NavLink></li>
          <li><i className="bx bx-chevron-right"></i> <NavLink to="/about">About Us</NavLink></li>
          <li><i className="bx bx-chevron-right"></i> <NavLink to="/courses">Courses</NavLink></li>
          <li><i className="bx bx-chevron-right"></i> <NavLink to="/terms">Terms of Service</NavLink></li>
          <li><i className="bx bx-chevron-right"></i> <NavLink to="/privacy">Privacy Policy</NavLink></li>
        </ul>
      </div>

     

      <div className="col-lg-4 col-md-6 footer-newsletter">
        <h4>Join Our Newsletter</h4>
        <p>Sign up today for hints, tips and the latest courses</p>
        <form action="" method="post">
          <input type="email" name="email"/>
          <input type="submit" value="Subscribe"/>
        </form>
      </div>

    </div>
  </div>
</div>

<div className="container py-4">

  <div className="me-md-auto text-left text-md-start">
    <div className="copyright">
    Copyright <strong><span>Aspire IT Academy</span></strong>. Promoted by <a target="_blank"  href="https://cydeztechnologies.com">CYDEZ TECHNOLOGIES</a>
    </div>
   
  </div>
  <div className="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="https://www.facebook.com/aspireacademykochi" className="facebook" target="_blank"><i className="fa fa-facebook"></i></a>
    <a  className="instagram"><i className="fa fa-instagram"></i></a>
    <a  className="google-plus"><i className="fa fa-skype"></i></a>
    <a  className="linkedin"><i className="fa fa-linkedin"></i></a>
  </div>
</div>
</footer>
        </div>
    );
}

export default Footer;