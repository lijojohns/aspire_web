import React, { Component } from "react";
import './styles.scss';
import {
  MDBContainer,
  MDBTabPane,
  MDBTabContent,
  MDBNav,
  MDBNavItem,
  MDBNavLink,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBIcon,
} from "mdbreact";

class PillsWithinTheTabs extends Component {
  state = {
    activeItemOuterTabs: "1",
    activeItemInnerPills: "1",
  };

  toggleOuterTabs = tab => e => {
    if (this.state.activeItemOuterTabs2 !== tab) {
      this.setState({
        activeItemOuterTabs: tab
      });
    }
  };

  toggleInnerPills = tab => e => {
    if (this.state.activeItemInnerPills !== tab) {
      this.setState({
        activeItemInnerPills: tab
      });
    }
  };

  render() {
    return (
      <MDBContainer>
        {/* <h2 className="mt-5">Pills within the tabs</h2> */}
        <MDBNav tabs className="nav-justified" color="primary">
         
          <MDBNavItem>
            <MDBNavLink link to="#" active={this.state.activeItemOuterTabs === "2"} onClick={this.toggleOuterTabs("2")} role="tab" >
              {/* <MDBIcon icon="heart" />  */}
              Python Basics
            </MDBNavLink>
          </MDBNavItem>
          <MDBNavItem>
            <MDBNavLink link to="#" active={this.state.activeItemOuterTabs === "3"} onClick={this.toggleOuterTabs("3")} role="tab" >
              {/* <MDBIcon icon="heart" />  */}
Job Opportunities            </MDBNavLink>
          </MDBNavItem>
        </MDBNav>
        <MDBTabContent
          className="card mb-5"
          activeItem={this.state.activeItemOuterTabs}
        >
      
          <MDBTabPane tabId="2" role="tabpanel">
            <MDBRow>
              <MDBCol md="12">
                <MDBCardBody>
                  <MDBCardTitle>Python & Advanced Python</MDBCardTitle>
                  <MDBCardText>
                  <p>Python is one of the most popular programming languages among I.T. professionals and data scientists. And it involves every aspect from web development to data visualization. Python is a prominent programming language which comes with compelling semantics that will help in building websites and apps. An essential feature of python which students would learn from the One to One Training by Aspire IT Academy is that python has been designed for easy readability.
</p><p>The python course details are in sync with the latest technology. The experienced members of Aspire IT Academy ensure that with the help of this course, learners can easily get access to some of the best jobs in the country.</p>
                  </MDBCardText>
                  {/* <MDBBtn>Go somewhere</MDBBtn> */}
                </MDBCardBody>
              </MDBCol>
              {/* <MDBCol md="6">
                <MDBCardBody>
                  <MDBCardTitle>Special Title Treatment</MDBCardTitle>
                  <MDBCardText>
                    With supporting text below as a natural lead-in to
                    additional content.
                  </MDBCardText>
                  <MDBBtn>Go somewhere</MDBBtn>
                </MDBCardBody>
              </MDBCol> */}
            </MDBRow>
          </MDBTabPane>

          <MDBTabPane tabId="3" role="tabpanel">
            <MDBRow>
              <MDBCol md="12">
              <MDBCardBody>
                  <MDBCardTitle>Job Opportunity</MDBCardTitle>
                  <MDBCardText>
                  <p>Python is one of the most popular programming languages among I.T. professionals and data scientists. And it involves every aspect from web development to data visualization. Python is a prominent programming language which comes with compelling semantics that will help in building websites and apps. An essential feature of python which students would learn from the One to One Training by Aspire IT Academy is that python has been designed for easy readability.
</p><p>The python course details are in sync with the latest technology. The experienced members of Aspire IT Academy ensure that with the help of this course, learners can easily get access to some of the best jobs in the country.</p>
                  <p>Software industry
I.T. jobs
Scientific computing
Financial industry
Automation and monitoring jobs
Data analytics
Machine learning</p>
<p>With time, more job opportunities for python programmers are rising which are not only interesting but dynamic in nature and allows the students to execute them efficiently. Aspire IT Academy is one of the best Python training institute in Kochi.The python courses in Kerala by Aspire IT Academy will ensure that none of the students are deprived of lucrative job opportunities with adept training solutions.</p>
                  </MDBCardText>
                  {/* <MDBBtn>Go somewhere</MDBBtn> */}
                </MDBCardBody>
              </MDBCol>
              <MDBCol md="6">
             
              </MDBCol>
            </MDBRow>
          </MDBTabPane>


        </MDBTabContent>
      </MDBContainer>
    );
  }
}
export default PillsWithinTheTabs