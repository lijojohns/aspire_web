import React, { useEffect } from 'react';
import './style.scss';



function Privacy(props) {

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

    return (
        <div>
          <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Privacy Policy</h2>
      </div>
    </div>
    <section id="about" className="about">
      <div className="container" data-aos="fade-up">
   
        <div className="row">
       <p>The Privacy Policy of the Platform explains how we may use Your personal data, we will at all times respect and ensure adherence to the privacy policy, additionally various settings are provided to help You to be able to control the manner in which others may be able to view Your information as chosen by You to be displayed on Your profile and the manner in which You may have chosen to be contacted. Any feedback provided by a user shall be deemed as non-confidential to the user.</p>
        <p>This Policy is subject to the terms of the Site Policy (Terms of Use) of aspireitacademy.in and the Application. This policy applies to those who register on the Platform (as defined in the Terms of Use) provided by the aspireitacademy.in, in connection with use of our services, or whose information aspireitacademy.in otherwise receives in connection with its services (such as but not limited to contact information of individuals associated with aspireitacademy.in including partner colleges/ educational institutes etc.)</p>
        
        <h3>Information collection</h3>
        <br/>
        <p>We collect information about you or your usage to provide better services to all of our users. We collect information in the following ways:</p>
        <ul>
      <li>Many of our services require you to register/sign up for an account on aspireitacademy.in. When you do, we will ask for personal information, such as, but not limited to your name, email address and telephone number to create/update your account.</li>
<li>To provide you additional services, we may collect your profile information such as, but not limited to education background, work experience, date of birth.</li>
<li>We collect information about the services that you use and how you use them such as, but not limited to log information and location information.</li>
<li>We may collect your personal information such as but not limited to bank details, passport details to assist for study abroad program which includes services like Letter of recommendation, student profile, student visa and application to university programmes.</li>
<li>When you communicate with aspireitacademy.in or its Application or use the aspireitacademy.in platform to communicate with other Members (such as advertisers, colleges/ institutes, etc.), we collect information about your communication and any other additional information you choose to provide.</li>
        </ul>
        </div>
    

      </div>
    </section>
    
    
        </div>
    );
}

export default Privacy;