import React from 'react';
import './style.scss';
import { Helmet } from 'react-helmet';
import php_banner from './../../assets/img/php-banner.png';
import VerticalTabs from '../../components/Vtabs/component';


function Php(props) {
    return (
        <div>
        
            <Helmet>
                <meta charSet="utf-8" />
                <title>PHP Training Institute - Kochi - Software Training | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Our PHP Training Courses covers the fundamentals of PHP to the advanced level of the PHP framework such as CodeIgniter and Laravel. We provide hands-on training and helps you understand." />
            </Helmet>


      <div className="breadcrumbs" data-aos="fade-in">
        <div className="container">
          <h2>PHP</h2>
          <p>The PHP courses in Kochi by Aspire IT are the basic fundamentals from website development with the help of HTML to advanced PHP securities. Our hands-on training involves website development with PHP, MySQL, and AJAX technologies.
          </p>
        </div>
      </div>



    <section id="course-details" className="course-details">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-8">
            <img src={php_banner} className="img-fluid msodm" alt=""/>
            <h3>PHP TRAINING AT ASPIRE IT ACADEMY</h3>
            <p>
            Aspire IT Academy offers PHP training course, Online/Offline, and we cover the fundamentals of PHP to advanced level and the PHP framework, such as CodeIgnitor and Laravel. We provide hands-on training and help you understand and learn how to apply PHP programming to real-world applications. CodeIgnitor and Laravel are a high-level PHP web framework that enables the rapid development of secure and maintainable websites. Built by experienced developers, these frameworks take care of much of the hassle of web development, so you can focus on writing your app without needing to reinvent the wheel. It is free and open-source, has a thriving and active community, and has significant documentation for support. 
            Now you can learn PHP programming from the best training institute, Aspire IT Academy Kochi, Ernakulam, Kerala.
            </p>
            <p>
            We assure 100% career guidance and placement support for all our students.
            </p>
    
</div>
          <div className="col-lg-4">

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Trainer</h5>
              <p><a href="trainers.html">Ammu Rajan</a></p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Fee</h5>
              <p>₹ 18000</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Available Seats</h5>
              <p>30</p>
            </div>

              <div className="course-info d-flex justify-content-between align-items-center">
              
              <p>Batch Schedule <br /> 9.00 AM | 11.00 AM | 2.00 PM | 4.00 PM | 6.00 PM | 8.00 PM</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Duration</h5>
              <p>3 Months</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>EMI Available</h5>
              <p>7000/- Month</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Certificate on Course accomplishment</h5>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Resume Preparation</h5>
            </div>
                            
            <a href="./syllabus/Python_Aspire.pdf"  className="button2 left-btn" download>
                <span>
                 
                  <span className="download-icon">
                    <i class="fa fa-download" aria-hidden="true"></i>
                  </span> 
                  Download E-Book
                </span>
            </a>

          </div>
        </div>

      </div>
    </section>

    



        </div>
    );
}

export default Php;