import React from 'react';
import { NavLink } from 'react-router-dom'
import './style.scss';


import php from './../../assets/img/php.png';
import machine1 from './../../assets/img/machine_learning.jpeg';
import python from './../../assets/img/python.png';
import android1 from './../../assets/img/android1.jpg';



function Popular(props) {
    return (
        <section id="popular-courses" className="courses">
        <div className="container" data-aos="fade-up">
  
          <div className="section-title">
            <h2>Courses</h2>
            <p>HOT Placement Courses</p>
          </div>
  
          <div className="row" data-aos="zoom-in" data-aos-delay="100">
  
          <div className="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
              <div className="course-item">
              <NavLink to="/python">  <img src={python} className="img-fluid" alt="..."/>
                <div className="course-content">
                  <div className="d-flex justify-content-between align-items-center mb-3">
                    <h4>Python</h4>
                    <p className="price">₹ 18000</p>
                  </div>
  
                  {/* <h3> Python</h3> */}
                  <p> We provide hands-on training and helps you understand and learn how to apply Python programming to real-world applications</p>
                  <div className="trainer d-flex justify-content-between align-items-center">
                    <div className="trainer-profile d-flex align-items-center">
                      
                      <div className="Anju-Prasad"></div>
                      <NavLink to="/trainers"> <span>Anju Prasad</span></NavLink>
                    </div>
                  </div>
                </div>
                </NavLink>
              </div>
            </div>
  
   
  
            <div className="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
              <div className="course-item">
              <NavLink to="/machine">   <img src={machine1} className="img-fluid" alt="..."/>
                <div className="course-content">
                  <div className="d-flex justify-content-between align-items-center mb-3">
                    <h4>.NET Core</h4>
                    <p className="price">₹ 18000</p>
                  </div>
  
                  {/* <h3> Machine Learning</h3> */}
                  <p>We provide hands-on training for Machine Learning and moduls you to become an expert In Machine Learning</p>
                  <div className="trainer d-flex justify-content-between align-items-center">
                    <div className="trainer-profile d-flex align-items-center">
                    <div className="Anju-Prasad"></div>
                      <NavLink to="/trainers"> <span>Anju Prasad  </span></NavLink>
                    </div>

                  </div>
                </div>
                </NavLink>
              </div>
            </div>
  
            <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
              <div className="course-item">
              <NavLink to="/php"> <img src={php} className="img-fluid" alt="..."/>
                <div className="course-content">
                  <div className="d-flex justify-content-between align-items-center mb-3">
                    <h4>Software Testing</h4>
                    <p className="price">₹ 18000</p>
                  </div>
  
                  {/* <h3> PHP</h3> */}
                  <p>Introduces the PHP framework and syntax, and covers in depth the most important techniques used to build dynamic web sites.</p>
                  <div className="trainer d-flex justify-content-between align-items-center">
                    <div className="trainer-profile d-flex align-items-center">
                      
                      <div className="Ammu-Rajan"></div>
                      <NavLink to="/trainers"> <span>Ammu Rajan  </span></NavLink>
                    </div>

                  </div>
                </div>
                </NavLink>
              </div>
            </div>
  
  
            <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
              <div className="course-item">
              <NavLink to="/php"> <img src={php} className="img-fluid" alt="..."/>
                <div className="course-content">
                  <div className="d-flex justify-content-between align-items-center mb-3">
                    <h4>PHP</h4>
                    <p className="price">₹ 18000</p>
                  </div>
  
                  {/* <h3> PHP</h3> */}
                  <p>Introduces the PHP framework and syntax, and covers in depth the most important techniques used to build dynamic web sites.</p>
                  <div className="trainer d-flex justify-content-between align-items-center">
                    <div className="trainer-profile d-flex align-items-center">
                    <div className="Ammu-Rajan"></div>
                      <NavLink to="/trainers"> <span>Ammu Rajan  </span></NavLink>
                    </div>

                  </div>
                </div>
                </NavLink>
              </div>
            </div>
          </div>
  
        </div>




        <div className="container" data-aos="fade-up">
  
  <div className="section-title">
    {/* <h2>Courses</h2>
    <p>HOT Placement Courses</p> */}
  </div>

  <div className="row" data-aos="zoom-in" data-aos-delay="100">

  <div className="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
      <div className="course-item">
      <NavLink to="/python">  <img src={python} className="img-fluid" alt="..."/>
        <div className="course-content">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h4>React Js</h4>
            <p className="price">₹ 18000</p>
          </div>

          {/* <h3> Python</h3> */}
          <p> We provide hands-on training and helps you understand and learn how to apply Python programming to real-world applications</p>
          <div className="trainer d-flex justify-content-between align-items-center">
            <div className="trainer-profile d-flex align-items-center">
            <div className="Anju-Prasad"></div>
              <NavLink to="/trainers"> <span>Anju Prasad</span></NavLink>
            </div>

          </div>
        </div>
        </NavLink>
      </div>
    </div>



    <div className="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
      <div className="course-item">
      <NavLink to="/machine">   <img src={machine1} className="img-fluid" alt="..."/>
        <div className="course-content">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h4>Angular Js</h4>
            <p className="price">₹ 18000</p>
          </div>

          {/* <h3> Machine Learning</h3> */}
          <p>We provide hands-on training for Machine Learning and moduls you to become an expert In Machine Learning</p>
          <div className="trainer d-flex justify-content-between align-items-center">
            <div className="trainer-profile d-flex align-items-center">
              
              <div className="Niranjana-Sudan"></div>
              <NavLink to="/trainers"> <span>Niranjana S</span></NavLink>
            </div>

          </div>
        </div>
        </NavLink>
      </div>
    </div>

    <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
      <div className="course-item">
      <NavLink to="/php"> <img src={php} className="img-fluid" alt="..."/>
        <div className="course-content">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h4>Node Js</h4>
            <p className="price">₹ 18000</p>
          </div>

          {/* <h3> PHP</h3> */}
          <p>Introduces the PHP framework and syntax, and covers in depth the most important techniques used to build dynamic web sites.</p>
          <div className="trainer d-flex justify-content-between align-items-center">
            <div className="trainer-profile d-flex align-items-center">
            <div className="Ammu-Rajan"></div>
              <NavLink to="/trainers"> <span>Ammu Rajan  </span></NavLink>
            </div>

          </div>
        </div>
        </NavLink>
      </div>
    </div>


    <div className="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
              <div className="course-item">
              <NavLink to="/android"> <img src={android1} className="img-fluid" alt="..."/>
                            <div className="course-content">
                  <div className="d-flex justify-content-between align-items-center mb-3">
                    <h4>Flutter</h4>
                    <p className="price">₹ 18000</p>
                  </div>
  
                  {/* <h3> Android</h3> */}
                  <p>Android is an open source and Linux-based operating system for mobile devices such as smartphones and tablet computers.</p>
                  <div className="trainer d-flex justify-content-between align-items-center">
                    <div className="trainer-profile d-flex align-items-center">
                      {/* <img src={trainer4} className="img-fluid" alt=""/> */}
                      <div className="Ann-Treesa"></div>
                     <NavLink to="/trainers"> <span>Ann Treesa  </span></NavLink>
                    </div>
  
                  </div>
                </div>
                </NavLink>
              </div>
            </div>	
  </div>

</div>
      </section>
    );
}

export default Popular;