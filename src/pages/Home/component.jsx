import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom'
import './style.scss';
import Featured from './featured';
import Popular from './popular';
// import CarouselPage from '../../components/carousal/component';


function Home(props) {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])


    return (
        <div>
             <section id="about" className="about">

               {/* <CarouselPage /> */}
      <div className="container mt-4" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">

           
                            <div className="exper box-border-after background-section text-center">
                                <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2wl70ENHirc?autoplay=0&rel=0" frameborder="0" allow="accelerometer; autoplay; fullscreen;clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                            </div>
                       

          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h1>WELCOME TO <br />ASPIRE IT ACADEMY</h1>
          <br/>
            <p>Aspire IT academy is the best software training institute in Kochi for both software training, development, and testing. Our institute provides support in technologies like Python, Manual Software Testing, Automation Software Testing, .NET, Java, React Js, Angular Js, Node Js, PHP, Machine Learning, Data Science, Digital Marketing and tools like Selenium and performance testing tools like JMeter, Cucumber, and LoadRunner.</p>
            <div className="course-info d-flex justify-content-between align-items-center">
              
              <a href="./Aspire.pdf" className="button2" download>
          
                  <span>
                   DOWNLOAD <b>FREE</b> BOOKLET <i class="fa fa-download" aria-hidden="true"></i>

                  </span>
              </a>
   
     </div>

          </div>
        </div>

      </div>
    </section>
    <section id="counts" className="counts section-bg">
      
    <div className="container">

      <div className="row counters center-quote">
        <h4>"Chance favors the prepared mind"</h4>

      </div>

    </div>
  </section>

    <section id="why-us" className="why-us">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-4 d-flex align-items-stretch">
            <div className="content">
              <h3>Why Choose Aspire?</h3>
              <p>
              Formed by IT industry veterans with decades of industry experience, Aspire IT academy aims to create a generation of students with skills prominent in the software industry. Our skilled trainers will provide you hands-on classes with real-time projects, mock interviews, questions and answers, project case studies, and much more to make you a technical expert.
              </p>
              <p>
Our Placement Team is following the organization's HR and ensures the chances of getting a placement for you and reduces your hassle to find the opportunities.
              </p>
              <div className="text-center">
                <NavLink to="/about" className="more-btn">Learn More <i className="bx bx-chevron-right"></i></NavLink>
              </div>
            </div>
          </div>
          <div className="col-lg-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div className="icon-boxes d-flex flex-column justify-content-center">
              <div className="row">
                <div className="col-xl-4 d-flex align-items-stretch">
                  <div className="icon-box mt-4 mt-xl-0">
                  <i className="fa fa-book" aria-hidden="true"></i>
                    <h4>Exclusive Curriculam</h4>
                    <p> Expertise in IT consultancy for the IT companies, for their different working areas Web-app related.</p>
                  </div>
                </div>
                <div className="col-xl-4 d-flex align-items-stretch">
                  <div className="icon-box mt-4 mt-xl-0">
                  <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                    <h4>We Build Your Career</h4>
                    <p> We provide all kind of IT management services of in the according to on their organization prioritie.</p>
                  </div>
                </div>
                <div className="col-xl-4 d-flex align-items-stretch">
                  <div className="icon-box mt-4 mt-xl-0">
                  <i className="fa fa-certificate" aria-hidden="true"></i>
                    <h4>Experienced Trainers</h4>
                    <p> Our firm is expert to create an efficient for user interface that makes design user interaction lively.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>

<br/>
    
    
    <Popular />
    <Featured />
 

    
        </div>
    );
}

export default Home;