import React from 'react';
import { NavLink } from 'react-router-dom'


// import about from './../../assets/img/about.jpg';
import angular from './../../assets/img/Angular.png';
import node from './../../assets/img/node.png';

import pythona from './../../assets/img/pythona.png';
import phpa from './../../assets/img/phpa.png';
import androida from './../../assets/img/androida.png';

import reactandnative from './../../assets/img/reactandnative.png';
import selenium from './../../assets/img/selenium.png';
import ManualTesting from './../../assets/img/ManualTesting.png';

import Cucumber from './../../assets/img/Cucumber.png';
import seoa from './../../assets/img/seoa.png';
import java from './../../assets/img/java.png';
import net from './../../assets/img/net.png';
import data from './../../assets/img/data.png';
import machine from './../../assets/img/machine.png';
import flutter from './../../assets/img/flutter.png';

function Featured(props) {
    return (
        <section id="features" className="features">
      <div className="container" data-aos="fade-up">
     
      <div className="section-title">
          <h2>Courses</h2>
          <p>Courses We Offer</p>
        </div>
        
  

        <div className="row" data-aos="zoom-in" data-aos-delay="100">
          <div className="col-lg-3 col-md-4">
            <div className="icon-box">

              <img src={angular} className="img-techno" alt="..."/>
              <h3><NavLink to="/angular" > Angular JS</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div className="icon-box">

              <img src={node} className="img-techno" alt="..."/>
              <h3><NavLink to="/node" > Node</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div className="icon-box">

              <img src={pythona} className="img-techno" alt="..."/>
              <h3><NavLink  to="/python"> Python</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4 mt-lg-0">
            <div className="icon-box">
              <img src={phpa} className="img-techno" alt="..."/>

              <h3><NavLink  to="/php"> PHP</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={androida} className="img-techno" alt="..."/>

              <h3><NavLink  to="/android"> Android</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={reactandnative} className="img-techno" alt="..."/>
              <h3><NavLink  to="/reactnative"> React & React Native</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">

              <img src={selenium} className="img-techno" alt="..."/>
              <h3><NavLink  to="/testing"> Selenium</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">

               <img src={ManualTesting} className="img-techno" alt="..."/>
              <h3><NavLink  to="/testing"> Software Testing</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={Cucumber} className="img-techno" alt="..."/>
              <h3><NavLink  to="/testing"> Cucumber</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">

              <img src={seoa} className="img-techno" alt="..."/>
              <h3><NavLink  to="/seo">Digital Marketing</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={java} className="img-techno" alt="..."/>
              <h3><NavLink  to="/java"> Java</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={net} className="img-techno" alt="..."/>
              <h3><NavLink  to="/dotnet"> .Net</NavLink></h3>
            </div>
          </div>
          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={data} className="img-techno" alt="..."/>
              <h3><NavLink  to="/datascience"> Data Science</NavLink></h3>
            </div>
          </div> 

          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={flutter} className="img-techno" alt="..."/>
              <h3><NavLink  to="/flutter"> Flutter & Firebase</NavLink></h3>
            </div>
          </div>
          
           <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={machine} className="img-techno" alt="..."/>
              <h3><NavLink  to="/machine"> Machine Learning</NavLink></h3>
            </div>
          </div>

          <div className="col-lg-3 col-md-4 mt-4">
            <div className="icon-box">
              <img src={androida} className="img-techno" alt="..."/>

              <h3><NavLink  to="/android"> Flutter</NavLink></h3>
            </div>
          </div>
         

        </div>

      </div>
    </section>
    );
}

export default Featured;