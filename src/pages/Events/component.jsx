import React from 'react';
import './style.scss';
import e1 from './../../assets/img/events/01.webp';
import e2 from './../../assets/img/events/02.webp';
import e3 from './../../assets/img/events/03.webp';
import e4 from './../../assets/img/events/04.webp';
import e5 from './../../assets/img/events/05.webp';
import e6 from './../../assets/img/events/06.webp';
import e7 from './../../assets/img/events/07.webp';
import e8 from './../../assets/img/events/08.webp';
import e9 from './../../assets/img/events/09.webp';
import e10 from './../../assets/img/events/10.webp';

function Events(props) {
    return (
        <div>
          
          <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Gallery</h2>
        {/* <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p> */}
      </div>
    </div>
    <section id="events" className="events">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                  <img src={e1} alt="..."/>
              </div>
              {/* <div className="card-body">
                <h5 className="card-title">Best Performance Award</h5>
              </div> */}
            </div>
          </div>
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e2} alt="..."/>
              </div>
              {/* <div className="card-body">
                <h5 className="card-title">Entertainment</h5>
              </div> */}
            </div>

          </div>

          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e3} alt="..."/>
              </div>
              {/* <div className="card-body">
                <h5 className="card-title">Our Team</h5>
              </div> */}
            </div>

          </div>

          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e4} alt="..."/>
              </div>
              {/* <div className="card-body">
                <h5 className="card-title">Tea Time</h5>
              </div> */}
            </div>

          </div>

          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e5} alt="..."/>
              </div>
            </div>

          </div>
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e6} alt="..."/>
              </div>
            </div>
          </div>
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e7} alt="..."/>
              </div>
            </div>

          </div>
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e8} alt="..."/>
              </div>
            </div>
          </div>

          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e9} alt="..."/>
              </div>
            </div>

          </div>
          <div className="col-md-6 d-flex align-items-stretch">
            <div className="card">
              <div className="card-img">
                <img src={e10} alt="..."/>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>
        </div>
    );
}

export default Events;