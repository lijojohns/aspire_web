import React from 'react';
import { Helmet } from 'react-helmet';
import './style.scss';

import trainer1 from './../../assets/img/placed_students/student1.webp';
import trainer2 from './../../assets/img/placed_students/student2.webp';
import trainer3 from './../../assets/img/placed_students/student3.webp';
import trainer4 from './../../assets/img/placed_students/student4.webp';
import trainer5 from './../../assets/img/placed_students/student5.webp';
import trainer6 from './../../assets/img/placed_students/student6.webp';
import trainer7 from './../../assets/img/placed_students/student7.webp';
import trainer8 from './../../assets/img/placed_students/student8.webp';
import trainer9 from './../../assets/img/placed_students/student9.webp';
import trainer10 from './../../assets/img/placed_students/student10.webp';
import trainer11 from './../../assets/img/placed_students/student11.webp';
import trainer12 from './../../assets/img/placed_students/student12.PNG';
import trainer13 from './../../assets/img/placed_students/student13.PNG';
import placement_14 from './../../assets/img/placed_students/Nimmy_George.PNG';
import placement_15 from './../../assets/img/placed_students/Reshma.PNG';
import placement_16 from './../../assets/img/placed_students/student14.PNG';


function Placements(props) {
    return (
        <div>

<Helmet>
                <meta charSet="utf-8" />
                <title>Job Oriented Courses In Kochi - Software Training | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Our Mission is to Provide 100% Placements to Students those who enrol for our specialised courses. Our Placement assistance starts with Resume preparation." />
            </Helmet>


            <div className="breadcrumbs">
      <div className="container">
        <h2>LIST OF STUDENTS WHO GOT PLACEMENTS</h2>
        <p>Our mission is to provide 100% placements to students who enroll in our specialized courses. Our Placement assistance starts with Training, Mock Interviews, Aptitude Tests, Resume preparation, and Interviews. We will provide unlimited placement assistance till the student gets placed satisfactorily.</p>
      </div>
    </div>
    <section id="trainers" className="trainers">
      <div className="container" data-aos="fade-up">

        <div className="row" data-aos="zoom-in" data-aos-delay="100">

        <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={placement_14} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={placement_15} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={placement_16} className="img-fluid place" alt=""/>
            </div>
          </div>
        

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer1} className="img-fluid" alt=""/>
            </div>
          </div>


          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer5} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer6} className="img-fluid place" alt=""/>
            
            </div>
          </div>
          
          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer2} className="img-fluid" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer3} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer4} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer7} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer8} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer9} className="img-fluid place" alt=""/>
            </div>
          </div>
  
          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer10} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer11} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer12} className="img-fluid place" alt=""/>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div className="member">
            <img src={trainer13} className="img-fluid place" alt=""/>
            </div>
          </div>

         

        </div>

      </div>
    </section>
        </div>
    );
}

export default Placements;