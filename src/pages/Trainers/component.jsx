import React from 'react';
import { Helmet } from 'react-helmet';
import './style.scss';

import trainer1 from './../../assets/img/trainers/trainer-1.webp';
import trainer2 from './../../assets/img/trainers/trainer-4.webp';
import trainer3 from './../../assets/img/trainers/trainer-5.webp';
import trainer4 from './../../assets/img/trainers/trainer-60.webp';
import trainer5 from './../../assets/img/trainers/trainer-7.webp';

import nimisha from './../../assets/img/management/nimisha.webp';
import neethu from './../../assets/img/management/neethu.webp';
import reeba from './../../assets/img/management/reeba.webp';

function Trainers(props) {
  return (
    <div>

      <Helmet>
        <meta charSet="utf-8" />
        <title>Our Team - Software Training - ASPIRE IT ACADEMY</title>
        <meta name="description" content="Enroll, Learn, Grow and Repeat ! Get ready to achieve your learning goals with Aspire IT Academy." />
      </Helmet>


      <div className="breadcrumbs">
        <div className="container">
          <h2>Our Team</h2>
          <p>Enroll, Learn, Grow and Repeat ! Get ready to achieve your learning goals with Aspire IT Academy</p>
        </div>
      </div>

      <section className="mana">
        <div className="container">
          <div className="row">

            <div className="col-lg-12">
              <h3>Management </h3>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={reeba} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Reeba Lijo</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing Director</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={nimisha} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Nimisha Viswajith</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing Partner, Operations Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={neethu} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Neethu Vishnu</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing partner, HR Manager</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

      <div className="col-lg-12">
              <h3>Trainers </h3>
            </div>

      <section id="trainers" className="trainers">
        <div className="container" data-aos="fade-up">
          <div className="row" data-aos="zoom-in" data-aos-delay="100">
            <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div className="member">
                <img src={trainer1} className="img-fluid" alt="" />
                <div className="member-content">
                  <h4>Kukku P</h4>
                  <span>Manual/Automation Testing</span>
                  <p>
                    Having 5 years of IT experience as Practice Head in Functional and Non Functional areas of Software Testing and Training.
                  </p>
                  <div className="social">
                    <a href=""><i className="bi bi-twitter"></i></a>
                    <a href=""><i className="bi bi-facebook"></i></a>
                    <a href=""><i className="bi bi-instagram"></i></a>
                    <a href=""><i className="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div className="member">
                <img src={trainer2} className="img-fluid" alt="" />
                <div className="member-content">
                  <h4>Ammu</h4>
                  <span>PHP/Codeigniter/Laravel</span>
                  <p>
                    With over 4 years of experience in web development.expert in various technologies / languages / databases / frameworks like PHP, javascript, jquery, SQL, codeigniter, laravel.
                  </p>
                  <div className="social">
                    <a href=""><i className="bi bi-twitter"></i></a>
                    <a href=""><i className="bi bi-facebook"></i></a>
                    <a href=""><i className="bi bi-instagram"></i></a>
                    <a href=""><i className="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div className="member">
                <img src={trainer3} className="img-fluid" alt="" />
                <div className="member-content">
                  <h4>Anju Prasad</h4>
                  <span>Python/Machine Learning/Data Science</span>
                  <p>
                    With over 4 years of experience in web development.expert in various technologies / frameworks like Python, javascript, jquery, SQL, DJango, Machine Learning and Data Science.
                  </p>
                  <div className="social">
                    <a href=""><i className="bi bi-twitter"></i></a>
                    <a href=""><i className="bi bi-facebook"></i></a>
                    <a href=""><i className="bi bi-instagram"></i></a>
                    <a href=""><i className="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div className="member">
                <img src={trainer4} className="img-fluid" alt="" />
                <div className="member-content">
                  <h4>Ann Treesa</h4>
                  <span>React/.net/NodeJs</span>
                  <p>
                    More than 4 years of thorough industry experience as  technical trainer with hands on deliveries and expertise in mobile Applications.
                  </p>
                  <div className="social">
                    <a href=""><i className="bi bi-twitter"></i></a>
                    <a href=""><i className="bi bi-facebook"></i></a>
                    <a href=""><i className="bi bi-instagram"></i></a>
                    <a href=""><i className="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div className="member">
                <img src={trainer5} className="img-fluid" alt="" />
                <div className="member-content">
                  <h4>Niranjana</h4>
                  <span>Angular</span>
                  <p>
                    Has two-plus years of experience in the field of web UI development and hybrid app development. She is in the field of technical training since and an expert faculty in front end framework training.
                  </p>
                  <div className="social">
                    <a href=""><i className="bi bi-twitter"></i></a>
                    <a href=""><i className="bi bi-facebook"></i></a>
                    <a href=""><i className="bi bi-instagram"></i></a>
                    <a href=""><i className="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </section>



      {/* <section className="mana">
        <div className="container">
          <div className="row">

            <div className="col-lg-12">
              <h3>Student Counsellors </h3>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={reeba} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Reeba Lijo</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing Director</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={nimisha} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Nimisha Viswajith</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing Partner, Operations Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="news-card">
                <a href="#" className="news-card__card-link"></a>
                <img src={neethu} alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                  <h2 className="news-card__title">Neethu Vishnu</h2>
                  <div className="news-card__details-wrapper">
                    <p className="news-card__excerpt">Managing partner, HR Manager</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section> */}
    </div>
  );
}

export default Trainers;