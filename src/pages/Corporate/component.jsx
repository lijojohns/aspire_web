import React from 'react';
import './style.scss'
import { NavLink } from 'react-router-dom'

import courses from './../../assets/img/corporate/courses.png';
import unity from './../../assets/img/corporate/unity.JPG';
import emvigo from './../../assets/img/corporate/emvigo.png';
import kruxsoft from './../../assets/img/corporate/kruxsoft.JPG';
import stud4 from './../../assets/img/testimonials/S1.jpg';
import stud5 from './../../assets/img/testimonials/anu.jpg';


function Corporate(props) {
  return (
    <div className="testcss">
      <div className="breadcrumbs" data-aos="fade-in">
        <div className="container">
          <h2>Corporate Training</h2>
          <p>Join Aspire IT Academy, Kerala’s Best Software Training Institute in Kochi and experience the difference. Our training courses assures your employees industry-ready. For Corporate Customized Plans and Pricing - Contact us. +91 9947144333, 9947244333</p>
        </div>
      </div>





      <section id="about" className="about">
        <div className="container" data-aos="fade-up">

          <div className="row">
            <div className="col-lg-6 order-1 order-lg-2" data-aos="fade-right" data-aos-delay="100">
              <img src={courses} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
              <h3>Our Methodology</h3>
              <br />
              {/* <p className="font-italic">
              The IT industry account for nearly 70 per cent of campus recruitment in South India, but there is a huge disconnect between what this industry wants and what the universities trains the students for. We attempt to eliminate this gap. We offer courses that are specifically designed for not just getting a job, but for excelling in IT industry. Apart from the technical training, we also provide courses on communication skills and offer placement counselling.
            </p> */}
              <p>Our training methodology is designed by a team of experienced industry experts and instructors. It is a perfect balance between theory and practice followed by placement support and continuous guidance.We believe we all learn and understand things in a different way! That’s why we have setup our IT training services to standout from crowd.</p>
              <p>Currently the IT industry account for nearly 70 per cent of campus recruitment in South India, 
                but there is a huge disconnect between what this industry wants and what the universities trains the 
                students for. We attempt to eliminate this gap. Apart from the technical training, 
                we also provide courses on communication skills and offer placement counselling.</p>
              <div className="course-info d-flex justify-content-between align-items-center">

                <a href="./corporate_plans/CORPORATE_TRAINING.pptx" className="button2" download>

                  <span>
                    DOWNLOAD <b>CORPORATE</b> DECK <i class="fa fa-download" aria-hidden="true"></i>

                  </span>
                </a>

              </div>
              

            </div>
          </div>

          <h3> Our Key Features</h3>


          <div className="row">
            <div className="col-lg-6">
              <div className="container">
                <div className="list">
                  <div className="num">
                    <h3>Detailed Orientation to the new batch.</h3>
                  </div>
                  <div className="num">
                    <h3>Expectations and performance evalution process would be inform at the start.</h3>
                  </div>
                  <div className="num">
                    <h3>Syllabus based training and daily task to accomplish.</h3>
                  </div>
                  <div className="num">
                    <h3>Attendance monitoring and continues evaluation.</h3>
                  </div>



                </div>
              </div>
            </div>

            <div className="col-lg-6">
              <div className="container">
                <div className="list">
                  <div className="num">
                    <h3>Handouts for daily topics for future reference.</h3>
                  </div>
                  <div className="num">
                    <h3>Recorded sessions.</h3>
                  </div>

                  <div className="num">
                    <h3>100% hands-on projects for training.</h3>
                  </div>

                  <div className="num">
                    <h3>Weekly progress and Final review reports.</h3>
                  </div>


                </div>
              </div>
            </div>

            <p>
              We believe we all learn and understand things in a different way! That’s why we have setup our IT training services to standout from crowd.
            </p>
          </div>


          <br />


        </div>
      </section>
      <section id="counts" className="counts section-bg">
        <div className="container">

          <div className="row counters center-quote">

Quality is everyone's responsibility.

          </div>

        </div>
      </section>
      <section id="testimonials" className="testimonials">
        <div className="container" data-aos="fade-up">

          <div className="section-title">
            <h2>Testimonials</h2>
            <p>What Organisations SAY</p>
          </div>

          <div className="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
            <div className="swiper-wrapper">

              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={emvigo} className="testimonial-img" alt="" />
                    <h3>Emvigo Technologies</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      Corporate training provided by Aspire Team helps us to kick-start our campus newbie batch.
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>

              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={kruxsoft} className="testimonial-img" alt="" />
                    <h3>Kruxsoft Solutions</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      .Net Stack with professional curriculam impressed me to join-hands with Aspire IT Academy Team. They provided added values to the training by providing GIT, JIRA, Rest API, Postman and Coding Standards. Really Promising.
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>
              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={unity} className="testimonial-img" alt="" />
                    <h3>Unity Infotech Solutions (UIS) Global</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      Cross Training was always be a trouble to us when comes to our freshers recruitment. Aspire IT Academy, provides us a end-end solution for our candidates to overcome this.
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>




            </div>
            <div className="swiper-pagination"></div>
          </div>

        </div>
      </section>
    </div>
  );
}

export default Corporate;