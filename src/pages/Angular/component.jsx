import React from 'react';
import './style.scss';
import { NavLink } from 'react-router-dom';

import seo from './../../assets/img/angular.jpg';
import android1 from './../../assets/img/android1.jpg';


function Angular(props) {
    return (
        <div>
        
        <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Angular</h2>
        <p>Thorough Angular training course, will help in understanding and learning Angular, Interpreter, Conditional statements, Data Structures, Classes and the commonly used Angular framework Flask or Django. We provide hands-on training for Angular and moulds you to become an expert Angular Programmer.
        </p>
      </div>
    </div>

    <section id="course-details" className="course-details">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-8">
            <img src={seo} className="img-fluid" alt=""/>
            <h3>Angular</h3>
            <p>
              Our Angular training course covers the fundamentals of Angular to advanced level and the Angular framework Django. We provide hands-on training and helps you understand and learn how to apply Angular programming to real-world applications.Django is a high-level Angular web framework that enables rapid development of secure and maintainable websites. Built by experienced developers, Django takes care of much of the hassle of web development, so you can focus on writing your app without needing to reinvent the wheel. It is free and open source, has a thriving and active community, great documentation, and many options for free and paid-for support.
            </p>
            <p>
              <h4>Features of Angular 6</h4>
              Updated Angular Command Line Interface helps to add application features in command line
Updated Component Development Kit supports responsive web design
Updated Elements enables to use angular components in non-angular environment
Updated RxJS library allows to write asynchronous and callback based code in Angular
Bazel compiler enables rebuild only the updated portion of the code
Angular 6 included some animation improvements
            </p>
            <h4>
            Career options:
Angular TypeScript Developer
Senior Developer
UI Developer
MVC Developer with Angular JS
Software Engineer
Senior Node JS developer
            </h4>
          </div>
          <div className="col-lg-4">

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Trainer</h5>
              <p><a href="trainers.html">Anju Prasad</a></p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Fee</h5>
              <p>₹ 60000
</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Available Seats</h5>
              <p>30</p>
            </div>

              <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Schedule</h5>
              <p>5.00 pm - 7.00 pm</p>
            </div>

                   <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Duration</h5>
              <p>6 Months</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
                        {/* <li> */}
                            <a href="./../../assets/download/aspire_brochure .pdf" className="button2" download>
                                {/* <FontAwesomeIcon icon="download" /> */}
                                <span>
                                 Download the free course brochure <i class="fa fa-download" aria-hidden="true"></i>

                                </span>
                            </a>
                        {/* </li> */}
                   </div>

          </div>
        </div>

      </div>
    </section>
{/* 
    <section id="cource-details-tabs" className="cource-details-tabs">
      <div className="container" data-aos="fade-up">
        <div className="row">
          <div className="col-md-3">
            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a className="nav-link active" id="v-pills-one-tab" data-toggle="pill" href="#v-pills-one" role="tab" aria-controls="v-pills-one" aria-selected="true">Overview of Angular</a>
              <a className="nav-link" id="v-pills-two-tab" data-toggle="pill" href="#v-pills-two" role="tab" aria-controls="v-pills-two" aria-selected="false">Angular Basics</a>
              <a className="nav-link" id="v-pills-three-tab" data-toggle="pill" href="#v-pills-three" role="tab" aria-controls="v-pills-three" aria-selected="false">Angular Advanced</a>
            </div>
    </div>
          <div className="col-md-8 offset-md-1">
            <div className="tab-content" id="v-pills-tabContent">
              <div className="tab-pane fade show active" id="v-pills-one" role="tabpanel" aria-labelledby="v-pills-one-tab">
                
                <div id="accordion" role="tablist">
                  <div className="card">
                    <div className="card-header top-headline" role="tab" id="headingOne">
                      <h5>
                        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Who is Just Energy?
                        </a>
                      </h5>
                    </div>
                    <div id="collapseOne" className="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                      <div className="card-body">
                        <p>Just Energy is a leading energy retailer in North America and one of the largest energy providers bringing energy solutions, natural gas and electricity to approximately 2 million customers. Our natural gas and electricity supply plans provide innovative solutions that allow our customers to choose from an array of plans to suit their lifestyle and comfort levels with market knowledge that includes secured rates, variable rates and index commodity supply programs. Just Energy also provides green energy products that provide a real and convenient solution for consumers to offset the environmental impact associated with their everyday energy use.</p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" role="tab" id="headingTwo">
                      <h5>
                        <a className="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          What markets does Just Energy service?
                        </a>
                      </h5>
                    </div>
                    <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div className="card-body">
                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" role="tab" id="headingThree">
                      <h5>
                        <a className="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Is Just Energy licensed?
                        </a>
                      </h5>
                    </div>
                    <div id="collapseThree" className="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                      <div className="card-body">
                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade" id="v-pills-two" role="tabpanel" aria-labelledby="v-pills-two-tab">
                <p>Culpa dolor voluptate do laboris laboris irure reprehenderit id incididunt duis pariatur mollit aute magna pariatur consectetur. Eu veniam duis non ut dolor deserunt commodo et minim in quis laboris ipsum velit id veniam. Quis ut consectetur adipisicing officia excepteur non sit. Ut et elit aliquip labore Lorem enim eu. Ullamco mollit occaecat dolore ipsum id officia mollit qui esse anim eiusmod do sint minim consectetur qui.
                </p>
              </div>
              <div className="tab-pane fade" id="v-pills-three" role="tabpanel" aria-labelledby="v-pills-three-tab">
                <p>Eu dolore ea ullamco dolore Lorem id cupidatat excepteur reprehenderit consectetur elit id dolor proident in cupidatat officia. Voluptate excepteur commodo labore nisi cillum duis aliqua do. Aliqua amet qui mollit consectetur nulla mollit velit aliqua veniam nisi id do Lorem deserunt amet. Culpa ullamco sit adipisicing labore officia magna elit nisi in aute tempor commodo eiusmod.
                </p>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </section> */}

        </div>
    );
}

export default Angular;