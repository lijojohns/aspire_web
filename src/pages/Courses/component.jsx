import React, { useEffect } from 'react';
import './style.scss';
import { NavLink } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import php from './../../assets/img/php.png';
import seo from './../../assets/img/seo.jpg';
import python from './../../assets/img/python.png';
import android1 from './../../assets/img/android1.jpg';

import ManualTesting1 from './../../assets/img/SoftwareTesting.jpg';

import Cucumber from './../../assets/img/Cucumber.png';
import seoa from './../../assets/img/seoa.png';
import java from './../../assets/img/java.png';
import net from './../../assets/img/net.png';
import data from './../../assets/img/data.png';
import machine1 from './../../assets/img/machine_learning.jpeg';
import flutter from './../../assets/img/flutter.png';
import angular1 from './../../assets/img/angularjs.png';
import node1 from './../../assets/img/nodejs.png';
import reactjs from './../../assets/img/reactJs.png';

// import trainer1 from './../../assets/img/trainers/trainer.jpg';
// import trainer2 from './../../assets/img/trainers/trainer-b.jpg';
// import trainer3 from './../../assets/img/trainers/trainer-d.jpg';
// import trainer4 from './../../assets/img/trainers/trainer-e.png';
// import trainer5 from './../../assets/img/trainers/trainer-f.jpg';


import angular from './../../assets/img/Angular.png';
import node from './../../assets/img/node.png';

import pythona from './../../assets/img/pythona.png';
import phpa from './../../assets/img/phpa.png';
import androida from './../../assets/img/androida.png';

import reactandnative from './../../assets/img/reactandnative.png';
import selenium from './../../assets/img/selenium.png';
import ManualTesting from './../../assets/img/ManualTesting.png';

import machine from './../../assets/img/machine.png';
import Popular from '../Home/popular';
import Featured from '../Home/featured';

function Courses(props) {
  
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

    return (
        <div>

            <Helmet>
                <meta charSet="utf-8" />
                <title>Software Training - Online/Offline courses Kochi | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Learn PHP, Data Science, Python, Web Designing, Software Testing, SEO, Java, .NET courses.. best training and testing institute in kochi, 100% placement assistance." />
            </Helmet>


           <div className="breadcrumbs">
            <div className="container">
              <h2>Courses We Offered</h2>
              <p>Enroll, Learn, Grow and Repeat ! Get ready to achieve your learning goals with Aspire IT Academy - Online & Offline Courses</p>
            </div>
          </div>

          <Popular />
          <Featured />

   




  
        </div>
    );
}

export default Courses;