import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import './style.scss';

import e3 from './../../assets/img/testimonial-video/1.mp4';
import e4 from './../../assets/img/testimonial-video/2.mp4';
import e5 from './../../assets/img/testimonial-video/3.mp4';
import e6 from './../../assets/img/testimonial-video/4.mp4';
import e7 from './../../assets/img/testimonial-video/5.mp4';
import e8 from './../../assets/img/testimonial-video/6.mp4';
// import e9 from './../../assets/img/events/09.jpeg';
// import e10 from './../../assets/img/events/10.jpeg';

function Testimonial(props) {
    return (
        <div>
          
          <Helmet>
                <meta charSet="utf-8" />
                <title>Student Testimonials- Software Training Institute in Kochi - Software Training  | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Video Testimonial from Aspire IT Academy." />
            </Helmet>

          <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>TESTIMONIALS OF OUR STUDENTS</h2>
        {/* <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p> */}
      </div>
    </div>
    <section id="events" className="events">
      <div className="container" data-aos="fade-up">


      <div className="slider">

{/* <div className="buttons">
  <div className="previous"></div>
  <div className="next"></div>
</div> */}

<div className="slide">
  <div className="testimonial">
    <blockquote>“ I’ve been interested in coding for a while but never
      taken the jump, until now.
      I couldn’t recommend this course enough. I’m now in the job of my
      dreams and so excited about the future. ”</blockquote>
    <p className="author">SreeLakshmi
      {/* <span>UX Engineer</span> */}
    </p>
  </div>

  <div className="slider-img">
    <video width="400" height="360" controls>
  <source src={e3} type="video/mp4"/>
  <source src={e3} type="video/ogg"/>
  Your browser does not support HTML video.
</video>      
  </div>

</div>

<div className="slide">
<div className="slider-img">
    <video width="400" height="340" controls>
        <source src={e7} type="video/mp4"/>
        <source src={e7} type="video/ogg"/>
        Your browser does not support HTML video.
    </video>      
</div>
<div className="testimonial">
    <blockquote>“ If you want to lay the best foundation possible I’d
      recommend taking this course.
      The depth the instructors go into is incredible. I now feel so
      confident about
      starting up as a professional developer. ”</blockquote>
    <p className="author">Vysakh P
      {/* <span>Junior Front-end Developer</span> */}
    </p>
</div>
</div>

<div className="slide">
  <div className="testimonial">
    <blockquote>“ Excellent place to excel in IT career. Professional teaching and live project training help to boost candidates 
      skills. Also provide dedicated walk-in interviews
       to reputed companies in and around Infopark. Great mentor !!! ”</blockquote>
    <p className="author">Dildiya B
      {/* <span>UX Engineer</span> */}
    </p>
  </div>

  <div className="slider-img">
    <video width="400" height="360" controls>
  <source src={e5} type="video/mp4"/>
  <source src={e5} type="video/ogg"/>
  Your browser does not support HTML video.
</video>      
  </div>
</div>

<div className="slide">
<div className="slider-img">
    <video width="400" height="340" controls>
        <source src={e6} type="video/mp4"/>
        <source src={e6} type="video/ogg"/>
        Your browser does not support HTML video.
    </video>      
</div>
<div className="testimonial">
    <blockquote>“ It was the first time attending a free one week course. Fully an interactive and
       practice sessions from professional trainers. I think these kind of programms are a good opportunity 
      for the students who are come from low financial background to get some knowledge
       about unfamiliar technological areas”</blockquote>
    <p className="author">Varshana Jayaprakash
      {/* <span>Junior Front-end Developer</span> */}
    </p>
</div>
</div>

<div className="slide">
  <div className="testimonial">
    <blockquote>“ This course helped me to understand the basic concepts of Python. 
      Our mentor was very friendly and she was teaching things in a way that even a beginner can understand the session easily.
       The session was very informative and useful. Thank you Aspire team. ”</blockquote>
    <p className="author">Firdousy P E
      {/* <span>UX Engineer</span> */}
    </p>
  </div>

  <div className="slider-img">
    <video width="400" height="360" controls>
  <source src={e4} type="video/mp4"/>
  <source src={e4} type="video/ogg"/>
  Your browser does not support HTML video.
</video>      
  </div>
</div>

<div className="slide">
<div className="slider-img">
    <video width="400" height="340" controls>
        <source src={e8} type="video/mp4"/>
        <source src={e8} type="video/ogg"/>
        Your browser does not support HTML video.
    </video>      
</div>
<div className="testimonial">
    <blockquote>“ It was the first time attending a free one week course. Fully an interactive and
       practice sessions from professional trainers. I think these kind of programms are a good opportunity 
      for the students who are come from low financial background to get some knowledge
       about unfamiliar technological areas”</blockquote>
    <p className="author">Sony Rinil
      {/* <span>Junior Front-end Developer</span> */}
    </p>
</div>
</div>

</div>




        <div className="row">
        

        </div>

      </div>
    </section>
        </div>
    );
}

export default Testimonial;