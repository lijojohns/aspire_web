import React, { useEffect } from 'react';
import './style.scss';



function Terms(props) {

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

    return (
        <div>
          <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Terms & Conditions</h2>
      </div>
    </div>
    <section id="about" className="about">
      <div className="container" data-aos="fade-up">
     
        <div className="row">
          <p>The following terms and conditions, along with all other terms and legal notices located on www.aspireitacademy.in (collectively, "Terms"), govern Your use of www.aspireitacademy.in (the "Website"). The Website is owned and operated by Aspire IT (India) Limited (hereinafter referred to as “Company”).</p>
          <p>
            <p>These Terms of Use ("Terms") constitute a legally binding agreement ("Agreement") between the user who visits the Website in connection with use of our services, or whose information Company otherwise receives in connection with the services provided through its education business (“You”) and the Company governing Your access to and use of the Platform, including any subdomains thereof, and any other websites through which the Website makes its services available (collectively, "Site"), our mobile, tablet and other smart device applications, and application program interfaces (collectively, "Application"). The Application and Site are collectively termed as "Platform".</p>
        <strong>LIMITED LICENSE</strong> You are granted a limited, non-exclusive, revocable and non-transferable license to utilize and access the Site pursuant to the requirements and restrictions of these Terms of Use. Aspire IT Academy may change, suspend, or discontinue any aspect of the Site at any time. Aspire IT may also, without notice or liability, impose limits on certain features and services or restrict your access to all or portions of the Site. You shall have no rights to the proprietary software and related documentation, if any, provided to you in order to access the Site. Except as provided in the Terms of Use, you shall have no right to directly or indirectly, own, use, loan, sell, rent, lease, license, sublicense, assign, copy, translate, modify, adapt, improve, or create any new or derivative works from, or display, distribute, perform, or in any way exploit the Site, or any of its contents (including software) in whole or in part.
          </p>
        </div>
    

      </div>
    </section>
    
    
        </div>
    );
}

export default Terms;