import React, { useEffect } from 'react';
import './style.scss'
import { NavLink } from 'react-router-dom'
import { Helmet } from "react-helmet";

import about from './../../assets/img/about.webp';
import stud1 from './../../assets/img/testimonials/anushma.jpg';
import stud2 from './../../assets/img/testimonials/arishma.jpg';
import stud3 from './../../assets/img/testimonials/F1.jpg';
import stud4 from './../../assets/img/testimonials/S1.jpg';
import stud5 from './../../assets/img/testimonials/anu.jpg';


function About(props) {

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <div className="testcss">

            <Helmet>
                <meta charSet="utf-8" />
                <title>Contact Software Training Institute in Kochi | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Aspire IT Academy assures 100% Software Job Placements & focus on Skill Development courses. Training from Highly Experienced Trainers. We offer Internship programes." />
            </Helmet>

      <div className="breadcrumbs" data-aos="fade-in">
        <div className="container">
          <h2>KNOW ABOUT ASPIRE IT ACADEMY</h2>
          <p>Join Aspire IT Academy, Kerala’s Best Software Training Institute in Kochi and experience the difference. Any course at any time is our motto. Our training course assures 100% Software Job Placements. Contact for more details.</p>
        </div>
      </div>

      <section id="about" className="about">
        <div className="container" data-aos="fade-up">

          <div className="row">
            <div className="col-lg-6 order-1 order-lg-2" data-aos="fade-right" data-aos-delay="100">
              <img src={about} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
              <h3>Our Methodology</h3>
              <br />
              {/* <p className="font-italic">
              The IT industry account for nearly 70 per cent of campus recruitment in South India, but there is a huge disconnect between what this industry wants and what the universities trains the students for. We attempt to eliminate this gap. We offer courses that are specifically designed for not just getting a job, but for excelling in IT industry. Apart from the technical training, we also provide courses on communication skills and offer placement counselling.
            </p> */}
              <p>Our training methodology is moduled by a team of experienced industry experts and instructors. It is a perfect balance between theory and hands-on, followed by placement support and continuous guidance. We believe we all learn and understand things differently! That’s why we have set up our IT training services to stand out from the crowd.</p>
              <p>Currently, the IT industry accounts for nearly 70 percent of campus recruitment in South India. However, there is a demanded disconnect between what this industry wants and the academics. We attempt to eliminate this gap. We offer courses crafted for excelling in the IT industry.</p>
              <p>Apart from the technical training, we also provide courses on communication skills, Resume preparation, and placement support.</p>
              <h3> Our Key Features</h3>

            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="container">
                <div className="list">
                  <div className="num">
                    <h3>We do Individual and Corporate Training.</h3>
                  </div>
                  <div className="num">
                    <h3>Assured Hands-on Training on Live Project.</h3>
                  </div>
                  <div className="num">
                    <h3>100% Job Oriented Training & Skill Development.</h3>
                  </div>
                  <div className="num">
                    <h3>Trained by Trainers having Years of Experience.</h3>
                  </div>


                  <div className="num">
                    <h3>Course completion certificate</h3>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-6">
              <div className="container">
                <div className="list">
                  <div className="num">
                    <h3>All Courses are of demanded Technologies. </h3>
                  </div>
                  <div className="num">
                    <h3>The course will improve your skills.</h3>
                  </div>
                  <div className="num">
                    <h3>Internship Programes.</h3>
                  </div>
                  <div className="num">
                    <h3>Mock Interview and Placement Assistance</h3>
                  </div>

                  <div className="num">
                    <h3>Flexible Timings</h3>
                  </div>


                </div>
              </div>
            </div>

            {/* <p>
              We believe we all learn and understand things in a different way! That’s why we have setup our IT training services to standout from crowd.
            </p> */}
          </div>


          <br />


        </div>
      </section>
      <section id="counts" className="counts section-bg">
        <div className="container">

          <div className="row counters">

          We believe we all learn and understand things differently! That’s why we have set up our IT training services to stand out from the crowd. 
          <br />The course will improve your skills and confidence.

          </div>

        </div>
      </section>
      <section id="testimonials" className="testimonials">
        <div className="container" data-aos="fade-up">

          <div className="section-title">
            <h2>Testimonials</h2>
            <p>What Students SAY</p>
          </div>

          <div className="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
            <div className="swiper-wrapper">

              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={stud2} className="testimonial-img" alt="" />
                    <h3>Arishma</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      I have attended a free online class on Software Testing. Our mentor gave us  basic insights to this field.It was very informative and useful sessions.Thank you Aspire team for giving this opportunity.
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>

              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={stud5} className="testimonial-img" alt="" />
                    <h3>Anusree A</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      This course helped me to understand the basic concepts of testing. Our mentor was very friendly and she was teaching things in a way that even a beginner can understand the session easily. The session was very informative and useful. Thank you Aspire team.
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>
              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={stud1} className="testimonial-img" alt="" />
                    <h3>Anushma S</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      Coach is so good that most of the beginner's can understand. Starting from basics is free of cost but from the core session we have to pay & it seems it's worth it . Because many other institutes are charging more . What i felt about this academy is the coach is same as my mother tongue so it's easy for me to attend the session & she's fun to
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>

              {/* <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={stud4} className="testimonial-img" alt="" />
                    <h3>Sivaranjan</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      This Python online free course helped me improve my knowledge and
                      I'm also attending the Software Testing free course in online, the online class is excellent the ma'am  cleared each process in this section , i also improved my knowledge . thankyou Aspire IT Academy
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div> */}

              <div className="swiper-slide">
                <div className="testimonial-wrap">
                  <div className="testimonial-item">
                    <img src={stud3} className="testimonial-img" alt="" />
                    <h3>Firdousy P E</h3>
                    <p>
                      <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                      Free online class provided for python basics is very helpfull know how to learn and what is python and all.also notes where provided its very helpfull and it is up to date.thank you aspire team providing such an experiencing and learning platform free
                      <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                  </div>
                </div>
              </div>

            </div>
            <div className="swiper-pagination"></div>
          </div>

        </div>
      </section>
    </div>
  );
}

export default About;