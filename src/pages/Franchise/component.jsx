import React from 'react';
import './style.scss'

function Franchise(props) {
    return (
        <div>

           <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Start a Profitable Business with Us</h2>
        <p> We are glad to have you around.</p>
      </div>
    </div>

    <section id="contact" className="contact">
      <div data-aos="fade-up">
        Franchising is an opportunity to be a partner with us. Are you confident in training industry ? 
        Wish to start a training business ? High Motivated ? 
        Confident to build a team ?
        Come let's join together. That's it!. For more details and Terms and Condition. Contact - +91 9947144333
      </div>

      <div className="container" >

        <div className="row mt-5">

          <div className="col-lg-4">
            <div className="info">
              <div className="address">
              <i class="fa fa-map-marker" aria-hidden="false"></i>

                <h4>Location:</h4>
                <p> Innerspace, Kaloor ,
                  Ernakulam</p>
              </div>

              <div className="email">
              <i class="fa fa-envelope" aria-hidden="false"></i>
                <h4>Email:</h4>
                <p>hr@aspireitacademy.in</p>
              </div>

              <div className="phone">
              <i class="fa fa-phone" aria-hidden="false"></i>
                <h4>Call:</h4>
                <p>+91-9947244333</p>
              </div>

            </div>

          </div>

          <div className="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" className="php-email-form">
              <div className="row">
                <div className="col-md-6 form-group">
                  <input type="text" name="name" className="form-control" id="name" placeholder="Your Name" required/>
                </div>
                <div className="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" className="form-control" name="email" id="email" placeholder="Your Email" required/>
                </div>
              </div>
              <div className="form-group mt-3">
                <input type="text" className="form-control" name="subject" id="subject" placeholder="Subject" required/>
              </div>
              <div className="form-group mt-3">
                <textarea className="form-control" name="message" rows="5" placeholder="Message" required></textarea>
              </div>
              <div className="my-3">
                <div className="loading">Loading</div>
                <div className="error-message"></div>
                <div className="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div className="text-center"><button type="submit">Send Message</button></div>
            </form>

          </div>

        </div>

      </div>
    </section>

        </div>
    );
}

export default Franchise;