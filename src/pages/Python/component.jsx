import React from 'react';
import './style.scss';
import { NavLink } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import python_banner from './../../assets/img/python-banner.jpg';
import seo from './../../assets/img/seo.jpg';
import android1 from './../../assets/img/android1.jpg';
import TabPage from '../../components/Vtabs/component';


function Python(props) {
    return (
        <div>

<Helmet>
                <meta charSet="utf-8" />
                <title>Python Training Institute in Kochi - Software Training  | Ernakulam - ASPIRE IT ACADEMY</title>
                <meta name="description" content="Our Python training course covers the fundamentals of Python to advanced level of the Python framework such as Django, Flask. We provide hands-on training and that helps you to understand like a PRO." />
            </Helmet>
        
        <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>PYTHON TRAINING AT ASPIRE IT ACADEMY</h2>
        <p>Thorough Python training course, will help in understanding and learning Python, Interpreter, Conditional statements, Data Structures, Classes and the commonly used Python framework Flask or Django. We provide hands-on training for Python and moulds you to become an expert Python Programmer.
        </p>
      </div>
    </div>

    <section id="course-details" className="course-details">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-8">
            <img src={python_banner} className="img-fluid msodm"/>
            <h3>PYTHON TRAINING AT ASPIRE IT ACADEMY</h3>
            <p>
              Our Python training course covers the fundamentals of Python to advanced level and the Python framework Django. We provide hands-on training and helps you understand and learn how to apply Python programming to real-world applications.Django is a high-level Python web framework that enables rapid development of secure and maintainable websites. Built by experienced developers, Django takes care of much of the hassle of web development, so you can focus on writing your app without needing to reinvent the wheel. It is free and open source, has a thriving and active community, great documentation, and many options for free and paid-for support.
            </p>
            <p>Introduction to Python, PyCharm, Language Fundamentals, Conditional Statements, Looping, Control Statements, String Manipulation, Lists, Tuple, Dictionaries, Functions, Modules, Input-output, Exception Handling, OOPS Concepts, Regular Expressions, Multithreading, Functional Programming, Map, Reduce, Filter, Iter tools, Python to DB.</p>
      <p><h4>COURSE DETAILS</h4>

<p>This Python training course begins with the basic concepts of programming and works upward to full-fledged Python applications development.</p>

                           

<h4>PYTHON TRAINING</h4>

 1. Html, css, Javascript, jquery, Bootstrap<br/>

2. The Python Language Python Installation,IDLE, Python Basics, Sequences Functions and Modules Duration: 2 Weeks<br/>

3. Python for Developers Python Basics, Strings, Files, Classes and Objects Database programming, PyCharmJetBrains, PEP8, Generators&Iterators, Functional Programming Python Web framework – Django<br/>
<h4>Benefits for Students</h4>
          Fully Hands On Training Model<br/>
Training Delivered by Software Engineers<br/>
Full Fledged Job Assistance Till You Get Placed.<br/>
Real time Digital training<br/>
Most Advanced Curriculum<br/>
Lifetime Query Support<br/>
Access to Live Projects<br/>
24×7 Support<br/>
Course Completion Certificate
</p>
         
          </div>
          <div className="col-lg-4">

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Trainer</h5>
              <p><a href="trainers.html">Anju Prasad</a></p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Fee</h5>
              <p>₹ 60000

</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Available Seats</h5>
              <p>30</p>
            </div>

              <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Schedule</h5>
              <p> 2-3 Hours/day</p>
            </div>

                   <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Duration</h5>
              <p>6 Months, 6 Days a Week</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              
                            <a href="./syllabus/Python_Aspire.pdf" className="button2" download>
                        
                                <span>
                                 Download the free course brochure <i class="fa fa-download" aria-hidden="true"></i>

                                </span>
                            </a>
                 
                   </div>


          </div>
        </div>

      </div>
    </section>

<TabPage />

    {/* <section id="cource-details-tabs" className="cource-details-tabs">
      <div className="container" data-aos="fade-up">
        <div className="row">
          <div className="col-md-3">
            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a className="nav-link active" id="v-pills-one-tab" data-toggle="pill" href="#v-pills-one" role="tab" aria-controls="v-pills-one" aria-selected="true">Overview of Python</a>
              <a className="nav-link" id="v-pills-two-tab" data-toggle="pill" href="#v-pills-two" role="tab" aria-controls="v-pills-two" aria-selected="false">Python Basics</a>
              <a className="nav-link" id="v-pills-three-tab" data-toggle="pill" href="#v-pills-three" role="tab" aria-controls="v-pills-three" aria-selected="false">Python Advanced</a>
            </div>
    </div>
          <div className="col-md-8 offset-md-1">
            <div className="tab-content" id="v-pills-tabContent">
              <div className="tab-pane fade show active" id="v-pills-one" role="tabpanel" aria-labelledby="v-pills-one-tab">
                
                <div id="accordion" role="tablist">
                  <div className="card">
                    <div className="card-header top-headline" role="tab" id="headingOne">
                      <h5>
                        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        PYTHON-DJANGO-OVERVIEW
                        </a>
                      </h5>
                    </div>
                    <div id="collapseOne" className="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                      <div className="card-body">
                        <ul>
                          <li>
                         	Python is a simple and less complicated programming language to learn when compared to other scripting languages.</li>
                          <li>	Greater demand for Python developers in recent times and reduced availability of professionals assure more career opportunities.</li>
                          <li>	 There has emerged an exponential importance for Robotics, Artificial Intelligence, DevOps and Web Development which chose Python as the preferred programming language</li>
                          <li>	 Top rated firms like Facebook, Pinterest, Instagram, Disqus, The Washington Posts and other organizations including NASA use Python with Django.</li>
                          <li>	 If you are looking for a better career, this is the best time to get training in Python with Django. Because in the recent future, the web developers will test and ensure the mastering skills in Python and advanced frameworks, especially Django.</li>
                          <li>	 Helps in fetching up a career with various opportunities, even to initiate a start-up with own products and services.</li>
                          <li>	 Shorter codes and quick deployment made Python to be the best choice for start-ups and bootstrappers, and possesses an extra weightage over other server scripting languages like PHP, C and Java.</li>
                          <li>	 Compared to PHP, Python Django handles and defies security errors.</li>
                          <li>	 Indeed.com has reported that, just in the US a Django professional can earn an average salary up to $117,000 per annum.</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" role="tab" id="headingTwo">
                      <h5>
                        <a className="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Python with Django course objectives
                        </a>
                      </h5>
                    </div>
                    <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div className="card-body">
                        <p>Understand the core construction of Python
●	 Realise the status of Python as the best scripting language
●	 Explain the structure and applications of python
●	 Learn the objective types of Python like Lists, Tuples, Strings, Dictionaries etc.
●	 Knowing how Python as the best object-oriented programming language
●	 Understanding the fundamental concepts like Flow control and conditions, File handling, OOPs and Python modules.
●	 Learn how to handle exception and error
●	 Understand and create maps and websites in Django
●	 Handle Django models, REST framework, AJAX and DjangojQuey for creating websites and its other applications.
●	 Identify Django template system
●	 Learn the required processes to function class inheritance that helps in reusability
●	 Indexing and slicing of data in python
</p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" role="tab" id="headingThree">
                      <h5>
                        <a className="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Is Just Energy licensed?
                        </a>
                      </h5>
                    </div>
                    <div id="collapseThree" className="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                      <div className="card-body">
                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade" id="v-pills-two" role="tabpanel" aria-labelledby="v-pills-two-tab">
                <p>Culpa dolor voluptate do laboris laboris irure reprehenderit id incididunt duis pariatur mollit aute magna pariatur consectetur. Eu veniam duis non ut dolor deserunt commodo et minim in quis laboris ipsum velit id veniam. Quis ut consectetur adipisicing officia excepteur non sit. Ut et elit aliquip labore Lorem enim eu. Ullamco mollit occaecat dolore ipsum id officia mollit qui esse anim eiusmod do sint minim consectetur qui.
                </p>
              </div>
              <div className="tab-pane fade" id="v-pills-three" role="tabpanel" aria-labelledby="v-pills-three-tab">
                <p>Eu dolore ea ullamco dolore Lorem id cupidatat excepteur reprehenderit consectetur elit id dolor proident in cupidatat officia. Voluptate excepteur commodo labore nisi cillum duis aliqua do. Aliqua amet qui mollit consectetur nulla mollit velit aliqua veniam nisi id do Lorem deserunt amet. Culpa ullamco sit adipisicing labore officia magna elit nisi in aute tempor commodo eiusmod.
                </p>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </section> */}

        </div>
    );
}

export default Python;