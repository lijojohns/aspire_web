import React from 'react';
import './style.scss';
import { NavLink } from 'react-router-dom';

import seo from './../../assets/img/machine_learning.jpg';
import android1 from './../../assets/img/android1.jpg';


function Machine(props) {
    return (
        <div>
        
        <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Machine Learning</h2>
        <p>Begins with an introduction to Statistics, Probability, Python, and R programming. The student will then conceptualize Data Preparation, Data Cleansing, Exploratory Data Analysis, and Data Mining (Supervised and Unsupervised).
        </p>
      </div>
    </div>

    <section id="course-details" className="course-details">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-8">
            <img src={seo} className="img-fluid" alt=""/>
            <h2></h2>
            <p>This data science certification course follows the CRISP-DM Methodology. The premier modules are devoted to a foundational perspective of Statistics, Mathematics, Business Intelligence, and Exploratory Data Analysis. The successive modules deal with Probability Distribution, Hypothesis Testing, Data Mining Supervised, Predictive Modelling - Multiple Linear Regression, Lasso And Ridge Regression, Logistic Regression, Multinomial Regression, and Ordinal Regression. Later modules deal with Data Mining Unsupervised Learning, Recommendation Engines, Network Analytics, Machine Learning, Decision Tree and Random Forest, Text Mining, and Natural Language Processing. The final modules deal with Machine Learning - classifier techniques, Perceptron, Multilayer Perceptron, Neural Networks, Deep Learning Black-Box Techniques, SVM, Forecasting, and Time Series algorithms. This is the most enriching Data Science course in Kochi in terms of the array of topics covered.</p>
           <p>Express is a minimal and flexible Node.js application framework that offers a set of robust features for developing web applications and APIs. Become an expert in Node by enrolling in our Node.js training course and learn to use the Express framework for building web applications. With our hands-on, practical training in Node.js, Web developers will get a working knowledge in developing Node.js applications.</p>
            <p>
            <h4>Benefits for Students</h4>
            Course Duration:3 Months<br/>
Fully Hands On Training Model<br/>
Training Delivered by Software Engineers<br/>
Full Fledged Job Assistance Till You Get Placed.<br/>
Real time Digital training<br/>
Most Advanced Curriculum<br/>
Lifetime Query Support<br/>
Access to Live Projects<br/>
24×7 Support<br/>
Course Completion Certificate<br/>
            </p>
          </div>
          <div className="col-lg-4">

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Trainer</h5>
              <p><a href="trainers.html">Anju Prasad</a></p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Fee</h5>
              <p>₹ 60000
</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Available Seats</h5>
              <p>30</p>
            </div>

              <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Schedule</h5>
              <p>5.00 pm - 7.00 pm</p>
            </div>

                   <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Duration</h5>
              <p>6 Months</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
                        {/* <li> */}
                            <a href="./../../assets/download/aspire_brochure .pdf" className="button2" download>
                                {/* <FontAwesomeIcon icon="download" /> */}
                                <span>
                                 Download the free course brochure <i class="fa fa-download" aria-hidden="true"></i>

                                </span>
                            </a>
                        {/* </li> */}
                   </div>

          </div>
        </div>

      </div>
    </section>

    <section id="cource-details-tabs" className="cource-details-tabs">
      <div className="container" data-aos="fade-up">
        <div className="row">
          <div className="col-md-3">
            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a className="nav-link active" id="v-pills-one-tab" data-toggle="pill" href="#v-pills-one" role="tab" aria-controls="v-pills-one" aria-selected="true">Syllabus</a>
              <a className="nav-link" id="v-pills-two-tab" data-toggle="pill" href="#v-pills-two" role="tab" aria-controls="v-pills-two" aria-selected="false">Benefits for Students</a>
              <a className="nav-link" id="v-pills-three-tab" data-toggle="pill" href="#v-pills-three" role="tab" aria-controls="v-pills-three" aria-selected="false">Job Opportunities</a>
            </div>
    </div>
          <div className="col-md-8 offset-md-1">
            <div className="tab-content" id="v-pills-tabContent">
              <div className="tab-pane fade show active" id="v-pills-one" role="tabpanel" aria-labelledby="v-pills-one-tab">
                
                <div id="accordion" role="tablist">
                
               <h4>   Lesson 1: Getting Started</h4>
            <ul>
              <li>
              Introduction
              </li>
              <li>
              What is Node
              </li>  <li>
              Node Architecture
              </li>  <li>
              How Node Works
              </li>  <li>
              Installing Node
              </li>
              <h4>Lesson 2: Node Module System</h4> 
               <li>Introduction</li>
               <li>Global Object</li>
               <li>Modules</li>
               <li>Creating Modules</li>
               <li>Different Modules</li>
               <li>Event Module and Arguments</li>
               <h4>Lesson 3: Node Package Manager </h4> 
               <li>Introduction</li>
               <li>Package and using package.json</li>
               <li>Npm package and source control</li>
               <li>Operations using packages</li>
               <h4>Lesson 4 : RESTful API’s Using Express </h4>
               <p>Nodemon<br/>
Restful Services<br/>
Building basic web server<br/>
Handling HTTP Requests</p>
<h4>Lesson 5 :Express Detailed</h4>
<p>Middleware<br/>
Configurations<br/>
Setting environments<br/>
Authentication<br/>
Structuring Application</p>
<h4>Lesson 6:Asynchronous Javascript</h4>
<p>
Synchronous vs Asynchronous<br/>
Callbacks<br/>
Promises<br/>
Async Await</p>
<h4>
Lesson 7: CRUD Operations using Mongoose </h4>
<p>Introduction to MongoDB<br/>
Installing and connecting MongoDB<br/>
Models<br/>
Saving Documents<br/>
Querying<br/>
Counting<br/>
Pagination<br/>
Updating Documents<br/>
Removing Documents<br/>
Referencing Documents</p>
            </ul>
                </div>
              </div>
              <div className="tab-pane fade" id="v-pills-two" role="tabpanel" aria-labelledby="v-pills-two-tab">





Lesson 8 : Mongo Data Validation 

Validation
Validators, custom validators
Async Validators
Lesson 9 : Authentication and Authorization 

User Model, Registering Users
Using Lodash, JWT
Authorization
Middleware
Lesson 10 : Error Handling

Rejected Promises
Express error
Middleware
Removing Try Catch Blocks
Uncaught exceptions
Extracting Routes
Lesson 11 : Basic Testing and Deployment 

Test-driven development basics
Preparing App for Production
Heroku
Basic Git
Lesson 12 : Wrap Up
              </div>
              <div className="tab-pane fade" id="v-pills-three" role="tabpanel" aria-labelledby="v-pills-three-tab">
                <p>Node.js is becoming popular among corporate giants all across the globe. Today, it is used to build applications like social media apps, real-time tracking apps, video and text chat engines, online games, and collaboration tools. Professionals skilled in Node.js can opt for jobe roles like:
                </p>
                <p>NodeJS Developer
Full-Stack Developer
NodeJS UI Developer
Back End Developer
Web Developer</p>
<p>The team of experts at Aspire IT provide reason-based training in Node.js. It imparts practical as well as theoretical knowledge which gives the candidates a simplified learning experience.</p>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </section>

        </div>
    );
}

export default Machine;