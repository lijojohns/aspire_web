import React from 'react';
import './style.scss';
import { NavLink } from 'react-router-dom';
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faSearch } from "@fortawesome/free-solid-svg-icons";
import php_banner from './../../assets/img/software-testing-banner.jpg';
import seo from './../../assets/img/seo.jpg';
import android1 from './../../assets/img/android1.jpg';
import VerticalTabs from '../../components/Vtabs/component';
// import doc from './../../assets/img/letter_of_instructions.doc';


function Testing(props) {
    return (
        <div>
        
        <div className="breadcrumbs" data-aos="fade-in">
      <div className="container">
        <h2>Software Testing</h2>
        <p>Our extensive and comprehensive Software Testing Training includes everything you need to know to become a Software Tester.
        </p>
      </div>
    </div>

    <section id="course-details" className="course-details">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-8">
            <img src={php_banner} className="img-fluid msodm" alt=""/>
            <h3>Software Testing</h3>
            <p>
            Our Software Testing course is intended to acquaint you with the complete software testing life-cycle. You will learn various levels of testing, test environment setup, test case design technique, test data creation, test execution, bug reporting and other essential concepts of Software Testing.
<br/>Software testing is the way toward checking a framework to distinguish any mistakes, holes or missing prerequisite versus the genuine necessity. Testing is comprehensively arranged into two kinds - Structural Testing and Function Testing.
<br/>This course is primarily aimed at those learners interested in any of the following roles: Software Engineer, Software Engineer in Test, Test Automation Engineer

            </p>
            <h4>Benefits for Students</h4>
            <p>Course Duration:3 Months
Training Delivered by Software Engineers<br/>
Fully Hands On Training Model<br/>
Full Fledged Job Assistance Till You Get Placed.<br/>
Led by Industry Experts<br/>
Real time Digital training<br/>
Most Advanced Curriculum<br/>
Lifetime Query Support<br/>
Access to Live Projects<br/>
24×7 Support<br/>
Course Completion Certificate</p>
<h4>Job Opportunities</h4>
<p>With the rising demand for PHP programmers and professionals in the I.T. industry, the job opportunities are immense. Here are some of the lucrative job opportunities:</p>
          
          <p>Web designers<br/>
Web developers<br/>
Mobile app developers<br/>
Software developers</p>
<p>Aspire IT Academy aim to add high value in every student’s career with effective PHP training in Kochi by well-qualified professionals</p></div>
          <div className="col-lg-4">

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Trainer</h5>
              <p><a href="trainers.html">Kukku</a></p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Fee</h5>
              <p>₹ 60000</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Available Seats</h5>
              <p>30</p>
            </div>

              <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Schedule</h5>
              <p>5.00 pm - 7.00 pm</p>
            </div>

                   <div className="course-info d-flex justify-content-between align-items-center">
              <h5>Course Duration</h5>
              <p>6 Months</p>
            </div>

            <div className="course-info d-flex justify-content-between align-items-center">
                        {/* <li> */}
                            <a href="./../../assets/download/aspire_brochure .pdf" className="button2" download>
                                {/* <FontAwesomeIcon icon="download" /> */}
                                <span>
                                 Download the free course brochure <i class="fa fa-download" aria-hidden="true"></i>

                                </span>
                            </a>
                        {/* </li> */}
                   </div>

          </div>
        </div>

      </div>
    </section>

    <section id="cource-details-tabs" className="cource-details-tabs">
      <div className="container" data-aos="fade-up">
        <div className="row">
          <div className="col-md-3">
            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a className="nav-link active" id="v-pills-one-tab" data-toggle="pill" href="#v-pills-one" role="tab" aria-controls="v-pills-one" aria-selected="true">Overview of PHP</a>
              <a className="nav-link" id="v-pills-two-tab" data-toggle="pill" href="#v-pills-two" role="tab" aria-controls="v-pills-two" aria-selected="false">PHP Basics</a>
              <a className="nav-link" id="v-pills-three-tab" data-toggle="pill" href="#v-pills-three" role="tab" aria-controls="v-pills-three" aria-selected="false">PHP Advanced</a>
            </div>
    </div>
          <div className="col-md-8 offset-md-1">
            <div className="tab-content" id="v-pills-tabContent">
              <div className="tab-pane fade show active" id="v-pills-one" role="tabpanel" aria-labelledby="v-pills-one-tab">
                
                <div id="accordion" role="tablist">
                 
               <p>
                 <h4>  Manual Testing</h4>
               

Introduction<br/>
Software Testing Principles Quality Philosophy<br/>
Steps for Software Process Software Development Life Cycle Project<br/>
Management Requirement Management Configuration Management<br/>
Fundamentals of Testing<br/>
Testing Economics & Testing Cost Testing Techniques<br/>
Testing maturity Model<br/>
Test Level Criteria<br/>
Special Testing Types<br/>
Testing Methodology<br/>
Use Case Design<br/>
Testing Standards<br/>
Testing Process<br/>
Testing Metrics<br/>
Testing Reports<br/>
Web Testing<br/>
Testing New Technologies<br/>
<h4>Automation Testing</h4>

<h4>1. Module 1</h4>

Introduction to selenium<br/>
Selenium components<br/>
Selenium RC<br/>
Selenium Grid<br/>
Selenium IDE<br/>
Selenium WebDriver<br/>
Installation<br/>
Basic components of Selenium IDE<br/>
Scripting in Selenium<br/>
IDE-Record and Play in Selenium IDE<br/>
Suit Execution<br/>
Export the scripts in different formats<br/>
Elements inspection<br/>
Inspecting the Elements<br/>
<h4>2. Module 2</h4>

Introduction of WebDriver<br/>
Installation of JDK<br/>
Installation of Eclipse<br/>
Language bindings for Selenium<br/>
Creation of Selenium projects<br/>
Selenium WebDriver commands<br/>
Browser Commands<br/>
Navigation Commands<br/>
Web Elements Commands<br/>
Locators/Object identification methods<br/>
X-path and Dynamic xpath creation<br/>
Dropdown Commands<br/>
Assert/Verifying inSelenium WebDriver<br/>
MouseHovering<br/>
Drag and Drop<br/>
Keyboard Handling<br/>
Wait Commands<br/>
Switch Commands<br/>
Screenshot Capture in Selenium<br/>
<h4>3. Module 3</h4>

Test Automation Framework<br/>
Components<br/>
Advantages<br/>
Reporting Mechanism<br/>
Framework-TestNG<br/>
Importance of TestNG<br/>
First Scenario in TestNG<br/>
Sequencing and Parametrization<br/>
Report generation in framework<br/>
annotations in TestNG<br/>
TestNG Suite creation<br/>
Parallel Testing using TestNG<br/>
Data Driven Framework<br/>
Excel sheet import<br/>
Page Object model framework<br/>
Introduction to Maven<br/>
Setting up of Maven in windows and Workspace<br/>
Creation and working with Maven<br/>
Projects in 2 ways- Converting the existing java project framework to Maven framework<br/>
Running the Maven projects<br/>
Introduction to Appium and Selenium Mobile Testing<br/>
Installation steps for Appium<br/>
Inspecting elements in Mobile App and Mobile Browser(Chrome)<br/>
Execution of Tests in mobile app and browser<br/>
Automate your Application<br/>
 

<h4>4. Module 4</h4>

Performance testing using JMeter
</p>
              
                </div>
              </div>
              <div className="tab-pane fade" id="v-pills-two" role="tabpanel" aria-labelledby="v-pills-two-tab">
                <p>Culpa dolor voluptate do laboris laboris irure reprehenderit id incididunt duis pariatur mollit aute magna pariatur consectetur. Eu veniam duis non ut dolor deserunt commodo et minim in quis laboris ipsum velit id veniam. Quis ut consectetur adipisicing officia excepteur non sit. Ut et elit aliquip labore Lorem enim eu. Ullamco mollit occaecat dolore ipsum id officia mollit qui esse anim eiusmod do sint minim consectetur qui.
                </p>
              </div>
              <div className="tab-pane fade" id="v-pills-three" role="tabpanel" aria-labelledby="v-pills-three-tab">
                <p>Eu dolore ea ullamco dolore Lorem id cupidatat excepteur reprehenderit consectetur elit id dolor proident in cupidatat officia. Voluptate excepteur commodo labore nisi cillum duis aliqua do. Aliqua amet qui mollit consectetur nulla mollit velit aliqua veniam nisi id do Lorem deserunt amet. Culpa ullamco sit adipisicing labore officia magna elit nisi in aute tempor commodo eiusmod.
                </p>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </section>

<div className="container">
  <VerticalTabs />
</div>
   

        </div>
    );
}

export default Testing;